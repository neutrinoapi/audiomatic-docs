Heroku Add-on
===============

Provisioning the add-on
-----------------------

Audiomatic can be attached to a Heroku application via the CLI:

.. code-block::


    $ heroku addons:create neutrino-audiomatic

    -----> Adding neutrino-audiomatic to sharp-mountain-4005... done, v18 (free)


Once Neutrino Audiomatic has been added a ``NEUTRINO_AUDIOMATIC_URL`` setting will be
available in the app configuration and will contain the canonical URL
used to access the newly provisioned Neutrino Audiomatic service instance. This
can be confirmed using the ``heroku config:get`` command.


.. code-block::


    $ heroku config:get NEUTRINO_AUDIOMATIC_URL

    http://user:pass@instance.ip/resourceid

After installing Neutrino Audiomatic the application should be configured to
fully integrate with the add-on.


Using with Rails 4.x, 5.x
-------------------------

Ruby on Rails applications will need to add the following entry into
their ``Gemfile`` specifying the Neutrino Audiomatic client library.

.. code-block:: ruby


      gem 'neutrino-audiomatic-rails'

Update application dependencies with bundler.

.. code-block::


      $ bundle install

Configuration
~~~~~~~~~~~~~

For heroku users, configuration is really easy.Just install Neutrino Audiomatic
(url) plugin and paste code-block below to
``config/initializer/neutrino_audiomatic_config.rb``:

.. code-block:: ruby


      NeutrinoAudiomatic.config do |c|

        c.processor_host = ENV['NEUTRINO_AUDIOMATIC_URL']

      end

You can also provide your own unique url:

.. code-block:: ruby


      NeutrinoAudiomatic.config do |c|

        c.processor_host = "https://login:password@neutrino-audiomatic.radiokitapp.org/api/process"

      end

Using with ActiveRecord
~~~~~~~~~~~~~~~~~~~~~~~

1. Include NeutrinoAudiomaticRails::Model in your Model

.. code-block:: ruby


      class YourModel < ApplicationRecord

        include NeutrinoAudiomaticRails::Model

      end

2. Tell Neutrino Audiomatic where your files are stored. Just pass method name to
   neutrino_audiomatic\_file\_url

.. code-block:: ruby


      neutrino_audiomatic_file_url :file_url



          # Example method returning URL

      def file_url

        "www.example.org/files/#{file.slug}.mp3"

      end

3. Configure processors. Set field where result should be saved (don’t
   forget to about adding column to database):

.. code-block:: ruby


            # model should have 'duration_value' field

      analyze :duration, [:duration_value]

4. Keep adding processors:

.. code-block:: ruby


      analyze :duration, [:duration]

      analyze :tags, [:artist, :title]

5. Last step: tell Neutrino Audiomatic where results should be sent back. Add to
   your routes.rb:

.. code-block:: ruby


    # config/routes.rb



    Rails.application.routes.draw do

      neutrino_audiomatic_for(YourModel)

    end

And that’s it!



In short:

.. code-block:: ruby


    config/initializer/neutrino_audiomatic\_config.rb

    NeutrinoAudiomatic.config do \|c\|

    c.processor\_host = ENV[‘NEUTRINO_AUDIOMATIC\_URL’]

    end

    config/routes.rb


    Rails.application.routes.draw do

    neutrino-audiomatic\_for(YourModel)

    end

    your\_model.rb







.. toctree::
   :maxdepth: 3
