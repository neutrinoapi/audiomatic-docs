Welcome to Neutrino Audiomatic's documentation!
===============================================

Audiomatic provides power of professional music studio as a service.
Lightning fast generation of waveform? Check! Duration of audio file in millisecond precision? No problem.
Conversion of any obscure format to web-ready mp3? 3.. 2... 1... Done!
Say 'no' to hours of googling, installing hundreds of plugins or obscure software.
Leave these bad parts of audio editing to us, so you have time for the important stuff.

Audiomatic is accessible via an API and has supported client libraries for Ruby. More coming soon.

Contents
~~~~~~~~

.. toctree::
   :maxdepth: 4

   httpapi.rst
   rails.rst
   heroku.rst
   support.rst
