HTTP API
========


Scheduling file processing requires POSTing two parameters to audiomatic api:
- url - location of processed file,
- callback_url - location where results of analysis should be returned
as JSON.

Url depends on desired processor. Root of API2.0 requests:
- https://audiomatic.radiokitapp.org/api/process/v2.0
should be concatenated with processor's path:

===========  ===============================
Processors   Path
===========  ===============================
duration:    "/analysis/audio/duration",
replaygain:  "/analysis/audio/replaygain",
tags:        "/analysis/audio/tags",
webbrowser:  "/transcode/audio/webbrowser",
waveform:    "/visualisation/audio/waveform"
===========  ===============================

In short, to obtain duration, POST to:
- https://audiomatic.radiokitapp.org/api/process/v2.0/analysis/audio/duration

JSON body:
{
 "callback_url":"https://example.org/callback",
 "url":"https://example.org/file.mp3"
}

Don't forget about authentication. Use Basic Auth with login and token you obtained during registration.
You can use either Authorization Header or URL encoding. For details, see https://en.wikipedia.org/wiki/Basic_access_authentication#Client_side

NeutrinoAudiomatic Response
~~~~~~~~~~~~~~~~~~~~~~~~~~~

<!---
TODO: Describe both immediate response, with reference, and give specific examples of every processor.
Write about response audiomatic expects.
-->
After sends request to NeutrinoAudiomatic, server answers twice. First response will be sent immediately and it will contain status of request. Second, will be sent results of analyze.

First response:

If status is 202, mean that NeutrinoAudiomatic get your request and start working on it. Body of successful response contain  reference number which is unique attribute of single analyze.
Otherwise, something went wrong.  

Seccond response:

Results are in json similar to:

{"result"=>[{"value"=>"122323", "key"=>"duration"}]} # => duration

{"result"=>[{"value"=>"AC/DC", "key"=>"artist"}, {"value"=>"T.N.T", "key"=>"title"}]} # => tags

{"result"=>[{"value"=>"http://www.example.org/file.mp3", "key"=>"webbrowser"}]} # => webbrowser

{"result"=>[{"value"=>0.05550943315029144, "key"=>"replaygain-track-peak"}, {"value"=>20.32999999999999, "key"=>"replaygain-track-gain"}, {"value"=>89, "key"=>"replaygain-reference-level"}]} # => replay gain

{"result"=>[{"value"=>"http://www.example.org/file.png", "key"=>"waveform"}]} # => waveform

===========  =======================
Processor    Results
===========  =======================
duration     "duration" [ms]
replaygain   "replaygain-track-peak" 
             "replaygain-track-gain" 
             "replaygain-reference-level"
tags         all tags of audio file
webbrowser   url to mp3 file 
waveform     url to png file 
===========  =======================

