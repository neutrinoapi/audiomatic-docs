

NeutrinoAudiomaticRails
=======================

Ruby on Rails applications will need to add the following entry into
their ``Gemfile`` specifying the Audiomatic client library.

.. code-block:: ruby

      gem 'audiomatic-rails'

Update application dependencies with bundler.

.. code-block::


      $ bundle install

Configuration
~~~~~~~~~~~~~

For heroku users, configuration is really easy. Just install Neutrino_Audiomatic
(url) plugin and paste code-block below to
``config/initializer/audiomatic_config.rb``:

.. code-block:: ruby


      Audiomatic.config do |c|
        c.processor_host = ENV['AUDIOMATIC_URL']
        c.audiomatic_secret = ENV['AUDIOMATIC_SHARED_SECRET']
      end

you can also provide your own unique url:


.. code-block:: ruby

      Audiomatic.config do |c|
        c.processor_host = "https://login:password@audiomatic.radiokitapp.org/api/process"
        c.audiomatic_secret = ENV['AUDIOMATIC_SHARED_SECRET'] 
      end

also you have to set the routes default_url_options in environments/production.rb

.. code-block:: ruby
      Rails.application.routes.default_url_options[:host] = 'your_app_url'
      

Getting start
~~~~~~~~~~~~~
1. Store file which you want to analyze. This gem provides method `store` which simplifies it. Example of controller:

.. code-block:: ruby

  def create 
    @audio_file = AudioFile.new
    @audio_file.store params[:audio_file][:audio]
    @audio_file.save
  end  

2. Include AudiomaticRails::Model in your ActiveRecord Model

.. code-block:: ruby

      class YourModel < ApplicationRecord

        include AudiomaticRails::Model

      end

3. Configure processors. Set field where result should be saved (don't
   forget to adding column to database):

.. code-block:: ruby

      # model should have 'duration' field
      analyze :duration, [:duration]

      # or (if file is named different then result)
      analyze :duration {duration: {field: :duration_field}}

4. Keep adding processors:

.. code-block:: ruby


        analyze :duration, {duration: {transformation: to_second= }}
        analyze :tags, [:artist, :title]
        analyze :waveform, mount_on: :waveform
        analyze :browser, mount_on: :mp3_file
        analyze :replaygain, save_to: replaygain_method=



5.Last step: tell Audiomatic where results should be sent back. Add to
   your routes.rb:

.. code-block:: ruby

    # config/routes.rb

    Rails.application.routes.draw do

      audiomatic_for(YourModel)

    end



And that's it!

Sets analyze
~~~~~~~~~~~~

NeutrinoAudiomaticRails gives many possibilities of processors configuration which will be done on your files. Gem has a lot of default configuration, but also gives capabilities to customize it. Method `analyze` describes which result we want to get, and how the result is handled.

Saving directly to ActiveRecord field
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The most default option: Select processor and put result to Array. It saves results to fields in yourModel (name of result must be the same as name of field)

.. code-block:: ruby

      #processor #result
    analyze :duration, [:duration]

To customize this strategy you can use Hash instead of Array, and select extra options:

  * :field - allows select field for result
  * :transformation - allows convert result before saving to db, transformations methods have to be in Array 

Example:

.. code-block:: ruby  

  analyze :replaygain, {'replaygain-track-peak': {field: :replaygain_track_peak}, 'replaygain-track-gain': {field: :'replaygain_track_gain', transformation: [:to_s=]}}

  analyze :duration, {duration: {transformation: [:to_s=]}}

Handle answer
~~~~~~~~~~~~~

If you want to get all results from processor and save or interpret it on your own, you have to use option `:save_to` which lets you write own method that would get result and do everything you want.
Example:

.. code-block:: ruby

        #processor #result
    analyze :tags, save_to: :tags=

    def tags= result # result is json {'key': name, 'value': value } 
      #your code...
    end

With CarrierWave
~~~~~~~~~~~~~~~~

Because in some cases NeutrinoAudiomatic returns file, neutrino_audiomatic_rails provides easy way to store this file in your Uploader. Example:


.. code-block:: ruby

  mount_uploader :waveform, WaveformUploader
    mount_uploader :mp3_file, Mp3Uploader

    analyze :webbrowser, mount_on: :mp3_file
    analyze :waveform, mount_on: :waveform


It means `mount_on: :uploader` is equl to `self.uploader.store! file`.
Now, results are in your Uploader.

Stores files
~~~~~~~~~~~~

To analyze file NeutrinoAudiomatic needs an url to your file. Neutrino_audiomatic_rails provides `store` method which saves your file in tmp folder and creates routes to get it. It will be used during sending request to NeutrinoAudiomatic.

.. code-block:: ruby

  def create 
    @audio_file = AudioFile.new
    @audio_file.store params[:audio_file][:audio]
    @audio_file.save
  end  


If you already have audio file stored. you can provide your own method which would return url to file to analyze.

.. code-block:: ruby

  audiomatic_file_url :set_url
  def set_url
    #your code
  end


NeutrinoAudiomatic errors
~~~~~~~~~~~~~~~~~~~~~~~~~

Sending request to NeutrinoAudiomac starts after saving instance of yourModel. While sending request to NeutrinoAudiomatic something can go wrong. In default configuration if server sends response with status diffrent then 202, method in gem raises exception, rollbacks transaction and method `save` returns false.

Second strategy allows you write your own reaction on error. All you have to do is to write method and set it as audiomatic_error. This method needs to have one argument. If your method returns true, then gem uses ActiveJob and tries sending request to server again after 15 minutes. If method returns false then gem raises excepction and deletes transaction.

.. code-block:: ruby
  
  audiomatic_error :handle_error=

  def handle_error= error
    #your code
  end


Response authentication 
~~~~~~~~~~~~~~~~~~~~~~~

Response sent from NeutrinoAudiomatic can be verified by calculation a signature.

All response contain `HTTP_X_NEUTRINO_AUDIOMATIC_HMAC_SHA256` header which is generated using the app's shared secret and body of response.

To verify that the request come from NeutrinoAudiomatic, compute the HMAC digest and then compere value and `HTTP_X_NEUTRINO_AUDIOMATIC_HMAC_SHA256` header. If they the same, you can be sure the response come from NeutrinoAudiomatic.

Simple example: 


.. code-block:: ruby

  def self.verify_neutrino(data, hmac_header)
    secret = ENV['AUDIOMATIC_SHARED_SECRET']
    calculated_hmac = Digest::SHA1.hexdigest(data + ":" + secret)
    calculated_hmac == hmac_header
  end

  def self.verify_response request
    request.body.rewind
    data = request.body.read
    verified = verify_neutrino(data, request.headers["HTTP_X_NEUTRINO_AUDIOMATIC_HMAC_SHA256"])
  end


Gem neutrino_audiomatic_rails authenticates response for you 